package com.company;

import java.util.Comparator;

public class AstarResultSetComparator implements Comparator<AstarResultSet>{

    @Override
    public int compare(AstarResultSet t1, AstarResultSet t2) {
        if(t1.getDistanceTravelled() > t2.getDistanceTravelled())
            return 1;
        else if (t1.getDistanceTravelled() < t2.getDistanceTravelled())
            return -1;
        return 0;
    }
}
