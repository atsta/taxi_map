package com.company;

import java.util.ArrayList;

public class AstarResultSet {
    private State endingPoint;
    private ArrayList<Location> route = new ArrayList<>();
    private double distanceTravelled;
    private int id;

    public int getId() { return id; }

    public State getEndingPoint() { return endingPoint; }

    public ArrayList<Location> getRoute() { return route; }

    public double getDistanceTravelled() { return distanceTravelled; }

    //find the path
    private void analyzeResult() {
        distanceTravelled = endingPoint.getDistanceToHere();
        State current = endingPoint;
        Location dump = new Location(0.0, 0.0);
        while (!current.getLocation().equals(dump)) {
            route.add(0, new Location(current.getLocation()));  //add nodes to the path
            current = current.getPrevious();
        }
    }

    public AstarResultSet(State endingPoint, int id) {
        this.endingPoint = endingPoint;
        this.id = id;
        analyzeResult();
    }
}
