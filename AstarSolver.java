package com.company;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import static java.lang.System.exit;

public class AstarSolver {
    private Location startingPoint;
    private Location endingPoint;
    private HashMap<Location, ArrayList<Location>> myMap;
    private HashMap<Location, Double> visited = new HashMap<>();
    private Priority forehead;
    private int id;

    //AstarSolver constructor
    public AstarSolver(Location startingPoint, Location endingPoint, HashMap<Location, ArrayList<Location>> myMap, int id) {
        this.startingPoint = startingPoint;
        this.endingPoint = endingPoint;
        this.myMap = myMap;
        this.id = id;
    }

    public AstarResultSet doSolve() {
        ArrayList<Location> currentNeighbours = myMap.get(startingPoint);
        forehead = new Priority(10000);  //metopo anazitisis
        State currentState = new State(startingPoint, 0.0, endingPoint, new State(new Location(0.0,0.0), 0.0, endingPoint));
        visited.put(new Location(currentState.getLocation()), currentState.getHeuristic());
        forehead.addArrayList(currentNeighbours, currentState, endingPoint);
        while (!forehead.mylist.isEmpty()) {
            currentState = forehead.get();
            if(currentState.getLocation().equals(endingPoint)) {  //if it reaches the end
                return new AstarResultSet(currentState, id);
            }
            if (!visited.containsKey(currentState.getLocation()))   //if it is not visited
                visited.put(currentState.getLocation(), currentState.getHeuristic());
            currentNeighbours = new ArrayList<>(myMap.get(currentState.getLocation()));  //put the neighbors
            for (Location n : currentNeighbours) {
                if (visited.containsKey(n))  continue;  //if the neighbor is visited do nothing bcs it is already visited with less cost
                if (!forehead.contains(n)) {
                    forehead.add(new State(n, currentState.getDistanceToHere()+n.distance(currentState.getLocation()), endingPoint, currentState));
                    continue;
                }
                double old_heuristic = forehead.find(n);  //if the old heuristic is better than the current then do nothing
                if(old_heuristic < currentState.getDistanceToHere()+n.distance(currentState.getLocation())+n.distance(endingPoint)) continue;
                forehead.remove(n);   //else remove the old one
                forehead.add(new State(n, currentState.getDistanceToHere()+n.distance(currentState.getLocation()), endingPoint, currentState)); //and put the current one
            }
        }
        return new AstarResultSet(currentState, id);
    }
}
