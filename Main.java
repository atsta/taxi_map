package com.company;

import java.io.*;
import java.awt.Color;
import java.util.*;
import static java.lang.System.exit;

public class Main {

    public static Location findClosest(Location k, ArrayList<Location> positions) {
        double minDistance = Double.MAX_VALUE;
        Location result = new Location(0.0, 0.0);
        for(Location i: positions) {
            if(i.distance(k) < minDistance) {
                result = new Location(i);
                minDistance = i.distance(k);
            }
        }
        return result;
    }

    //read the nodes from the file and put them in a hashmap in proper neighborhoods
    //read the location of all available taxis from the file
    //read the client location from the file
    public static void main(String[] args) {

        ArrayList<Location> nodes = new ArrayList<>();  //an ArrayList to store all the Geo-Coordinates
        HashMap<Location, ArrayList<Location>> neighbors = new HashMap<>();  //put the neighbors in a map
        ArrayList<Node> taxis; //an ArrayList to store all the taxi locations
        Location client;

        //read the nodes
        try (BufferedReader br = new BufferedReader(new FileReader("nodes.csv"))) {
            String line = br.readLine();  //first line is useless
            line = br.readLine();  //read first node
            String[] str = line.split(","); // use comma as separator
            Node k = new Node(str);
            Location previous_loc = k.getLocation();
            nodes.add(previous_loc);
            int previous_id = k.getId();
            neighbors.put(previous_loc, new ArrayList<>());
            while ((line = br.readLine()) != null) {
                str = line.split(",");
                Node l = new Node(str);
                Location current_loc = l.getLocation();
                nodes.add(current_loc);
                Integer current_id = l.getId();
                if (previous_id == current_id) {   //neighbor case
                    neighbors.get(previous_loc).add(current_loc);   //add a neighbor
                    if (!neighbors.containsKey(current_loc)) {
                        neighbors.put(current_loc, new ArrayList<>());  //if a key-Location is not on the Map, it has to be initialized
                    }
                    neighbors.get(current_loc).add(previous_loc);
                } else {
                    if (!neighbors.containsKey(current_loc))
                        neighbors.put(current_loc, new ArrayList<>());
                }
                previous_loc = current_loc;
                previous_id = current_id;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //read the client location
        try (BufferedReader br = new BufferedReader(new FileReader("client.csv"))) {
            String line = br.readLine();
            line = br.readLine();
            String[] str = line.split(",");
            client = new Location(Double.parseDouble(str[1]), Double.parseDouble(str[0]));
        } catch (IOException e) {
            client = new Location(0.0, 0.0);
            e.printStackTrace();
        }

        //read the locations of the taxis
        try (BufferedReader br = new BufferedReader(new FileReader("taxis.csv"))) {
            taxis = new ArrayList<>();
            String line = br.readLine();
            String[] str;
            while ((line = br.readLine()) != null) {
                str = line.split(",");
                Node l = new Node(str);
                taxis.add(l);
            }
        } catch (IOException e) {
            taxis = new ArrayList<>();
            e.printStackTrace();
        }

        Location endingPoint = findClosest(client, nodes);  //find closest node to client
        AstarSolver mySolver;
        ArrayList<AstarResultSet> results = new ArrayList<>();
        for (Node i : taxis) {
            Location startingPoint = findClosest(i.getLocation(), nodes);
            mySolver = new AstarSolver(startingPoint, new Location(endingPoint), new HashMap<>(neighbors), i.getId());
            results.add(mySolver.doSolve());
        }

        results.sort(new AstarResultSetComparator());   //sort the results from A*

        System.out.println(results.get(0).getDistanceTravelled());
        kml("mykml.kml", results);
    }
    //kml structure
    private static void kml(String filename, ArrayList<AstarResultSet> results) {
        Random rand = new Random();
        Color green = Color.GREEN.darker(), color;
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(filename);
            writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            writer.println("<kml xmlns=\"http://earth.google.com/kml/2.1\">");
            writer.println("<Document>");
            writer.println("<name>Taxi Routes</name>");
            writer.println("<Style id=\"green\">");
            writer.println("<LineStyle>");
            writer.println("<color>" + Integer.toHexString(green.getRGB()) + "</color>");
            writer.println("<width>4</width>");
            writer.println("</LineStyle>");
            writer.println("</Style>");
            int i = 1;
            //Make random colors for the rest of the taxi routes. Don't make it green.
            while (i < results.size()) {
                int r = rand.nextInt(255);
                int g = rand.nextInt(127);
                int b = rand.nextInt(255);
                color = new Color(r, g, b);
                if (color.getRGB() == green.getRGB()) continue;

                writer.println("<Style id=\"taxi" + i + "\">");
                writer.println("<LineStyle>");
                writer.println("<color>" + Integer.toHexString(color.getRGB()) + "</color>");
                writer.println("<width>4</width>");
                writer.println("</LineStyle>");
                writer.println("</Style>");
                i++;
            }
            ArrayList<Location> current = results.get(0).getRoute();
            int id = results.get(0).getId();
            results.remove(0);
            //This is to make the client point
            writer.println("<Placemark>");
            writer.println("<name>Client</name>");
            writer.println("<Point>");
            writer.println("<coordinates>");
            writer.println(current.get(0));
            writer.println("</coordinates>");
            writer.println("</Point>");
            writer.println("</Placemark>");
            //This is to make the first route have green color.
            writer.println("<Placemark>");
            writer.println("<name>TaxiID " + id + "</name>");
            writer.println("<styleUrl>#green</styleUrl>");
            writer.println("<LineString>");
            writer.println("<altitudeMode>relative</altitudeMode>");
            writer.println("<coordinates>");
            for (Location co : current) writer.println(co);
            writer.println("</coordinates>");
            writer.println("</LineString>");
            writer.println("</Placemark>");
            i = 1;
            //Do the same thing for all the rest of the taxi routes.
            for(AstarResultSet currentSet : results) {
                ArrayList<Location> currentRoute = currentSet.getRoute();
                Location loc = currentRoute.get(0);
                currentRoute.remove(0);
                writer.println("<Placemark>");
                writer.println("<name>TaxiID " + currentSet.getId() + "</name>");
                writer.println("<styleUrl>#taxi" + i + "</styleUrl>");
                writer.println("<LineString>");
                writer.println("<altitudeMode>relative</altitudeMode>");
                writer.println("<coordinates>");
                for (Location co : currentRoute) writer.println(co);
                writer.println("</coordinates>");
                writer.println("</LineString>");
                writer.println("</Placemark>");
                i++;
            }
            writer.println("</Document>");
            writer.println("</kml>");
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}
