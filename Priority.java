package com.company;

import java.util.TreeSet;
import java.util.ArrayList;

public class Priority {
    public TreeSet<State> mylist = new TreeSet<>(new StateComparator());
    private int width;

    public void add(State G) {
        mylist.add(G);
        if(mylist.size() > width) mylist.pollLast();
    }

    public Priority(int width) { this.width = width; }

    //return the first element of the queue
    public State get() { return mylist.pollFirst(); }

    //check if the element is/is not in the queue
    public boolean contains(Location loc) {
        for (State i : mylist) {
            if(i.getLocation().equals(loc))
                return true;
        }
        return false;
    }

    //same with contains, but it also returns the specific element/ or 0.0 if it is not in the queue
    public double find(Location loc) {
        for (State i : mylist) {
            if(i.getLocation().equals(loc))
                return i.getHeuristic();
        }
        return 0.0;
    }

    //delete an element from the queue
    public void remove(Location loc) {
        for (State i : mylist) {
            if(i.getLocation().equals(loc)) {
                mylist.remove(i);
                break;
            }
        }
    }

    public void addArrayList(ArrayList<Location> geitones, State prev, Location end) {
        int i=0;
        for (Location n: geitones) {
            i++;
            State temp = new State(n, prev.getDistanceToHere() + n.distance(prev.getLocation()), end, prev);
            add(temp);
        }
    }
}
