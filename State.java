package com.company;

public class State{
    private Location loc;
    private double distanceToHere;
    private double distanceToEnd;
    private double heuristic;
    private State previous;

    public double getHeuristic() { return heuristic; }

    public double getX() { return loc.x; }

    public double getY() { return loc.y; }

    public Location getLocation() {return loc; }

    public double getDistanceToHere() { return distanceToHere; }

    public State getPrevious() { return previous; }

    //constructor no.1
    public State(Location start, double distanceToHere, Location end) {
        this.loc = new Location(start);
        this.distanceToHere = distanceToHere;
        this.distanceToEnd = end.distance(loc);
        this.heuristic = this.distanceToHere+this.distanceToEnd;
    }

    //constructor no.2
    public State(Location start, double distanceToHere, Location end, State prev) {
        this.loc = new Location(start);
        this.distanceToHere = distanceToHere;
        this.distanceToEnd = end.distance(loc);
        this.heuristic = this.distanceToHere+this.distanceToEnd;
        this.previous = prev;
    }
}
