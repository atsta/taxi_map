package com.company;
import java.util.Comparator;


public class StateComparator implements Comparator<State>{

    @Override
    public int compare(State t1, State t2) {
        if(t1.getHeuristic() > t2.getHeuristic())
            return 1;
        else
            return -1;

    }
}
